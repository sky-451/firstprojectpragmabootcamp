package com.johanB.firstProject.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserUtilsTest {

    @Test
    void validatePassword() {
        String validPassword = "asd123QWE-";
        String invalidPassword1 = "asd123QWE";
        String invalidPassword2 = "asd123qwe";
        String invalidPassword3 = "asd123";
        String invalidPassword4 = "asd123456789asd123";
        assertEquals(UserUtils.validatePassword(validPassword), true);
        assertEquals(UserUtils.validatePassword(invalidPassword1), false);
        assertEquals(UserUtils.validatePassword(invalidPassword2), false);
        assertEquals(UserUtils.validatePassword(invalidPassword3), false);
        assertEquals(UserUtils.validatePassword(invalidPassword4), false);

    }

    @Test
    void validateEmail() {
        String validEmail = "test.johan@hotmail.com";
        String invalidEmail1 = "alice..bob@example.com";
        String invalidEmail2 = "alice...bob@example.com";
        String invalidEmail3 = "'delete-tables'@example.com";
        String invalidEmail4 = "alice@.example.com";
        String invalidEmail5 = "alice.at.example.com";
        String invalidEmail6 = "alice.example.com";

        assertEquals(UserUtils.validateEmail(validEmail), true);
        assertEquals(UserUtils.validateEmail(invalidEmail1), false);
        assertEquals(UserUtils.validateEmail(invalidEmail2), false);
        assertEquals(UserUtils.validateEmail(invalidEmail3), false);
        assertEquals(UserUtils.validateEmail(invalidEmail4), false);
        assertEquals(UserUtils.validateEmail(invalidEmail5), false);
        assertEquals(UserUtils.validateEmail(invalidEmail6), false);
    }
}