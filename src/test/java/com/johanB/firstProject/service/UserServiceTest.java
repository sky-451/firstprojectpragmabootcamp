package com.johanB.firstProject.service;

import com.johanB.firstProject.model.User;
import com.johanB.firstProject.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
class UserServiceTest {

    @Autowired
    private UserRepository userRepository;
    @MockBean
    private UserService userService;

    @BeforeEach
    void init() {
        userRepository.deleteAll();
    }

    @Test
    void getUserByEmail() {
        //TODO: add testing of service and handle exceptions
    }

    @Test
    void createUser() {
        //TODO: add testing of service and handle exceptions
    }
}