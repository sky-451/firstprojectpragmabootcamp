package com.johanB.firstProject.utils;

import java.util.regex.Pattern;

public final class UserUtils {
    public static boolean validatePassword(String password) {
        // validating password
        if(password.length() < 8 || password.length() > 15 ) {
            return false;
        }

        String pattern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[*_-]).+$";
        boolean isValidPassword = Pattern.compile(pattern).matcher(password).matches();

        return isValidPassword;
    }

    public static boolean validateEmail(String email) {
        // validating email by OWASP validation regex repository
        String regexPattern = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        boolean isValidEmail = Pattern.compile(regexPattern).matcher(email).matches();

        return isValidEmail;
    }

}
