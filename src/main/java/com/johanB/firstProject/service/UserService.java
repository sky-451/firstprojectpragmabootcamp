package com.johanB.firstProject.service;

import com.johanB.firstProject.model.User;
import com.johanB.firstProject.repository.UserRepository;
import com.johanB.firstProject.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public User getUserByEmail(String email) throws Exception {
        try {
            User user = repository.findByEmail(email);

            if (user != null) {
                return user;
            } else {
                throw new Exception("User not found");
            }
        } catch (Exception e) {
            throw new Exception();
        }
    }

    public User createUser(User user) throws Exception {
        try {
            boolean isValidEmail = UserUtils.validateEmail(user.getEmail());
            boolean isValidPassword = UserUtils.validatePassword(user.getPassword());
            if (!isValidEmail || !isValidPassword) {
                throw new Exception("invalid fields");
            }

            User newUser = repository
                    .save(new User(user.getName(), user.getLastName(), user.getPhone(),
                            user.getAddress(), user.getEmail(), user.getPassword()));

            return newUser;
        } catch (Exception e) {
            throw new Exception();
        }
    }
}
